# Allgemeines
Dieses Repo beinhaltet organisatorische Dokumentation der Gruppe "Bits und Bäume Dresden".
Es ist lose inspiriert von <https://about.gitlab.com/blog/2019/01/15/git-for-business-processes/>.

Zusammenfassung:
- Die Regeln für alle organisatorischen Prozesse sind unter Versionskontrolle dokumentiert.
- Ergänzungen und Änderungen können niedrigschwellig und transprarent vorgenommen werden.

Motivation:
- Handhabung der Webseite (inkl. des Blogs)
- Dokumentation der Protokolle
- Festhalten unseres Selbstverständnisses
