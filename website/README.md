# Erzeugung der Webseite
## Automatische Builds (Master und Vorschau)

- Die Webseite besteht aus statischem HTML und wird mit jekyll basierend auf Markdown-Dateien und Templates gebaut.
- Jeder Commit in das Repo: <ssh://bubdd@kojima.uberspace.de/home/bubdd/git/dresden.bits-und-baeume.org.git/> löst eine Neuerzeugung aus.
- Das geschieht mittels eines Skriptes (git-hook: post-recieve).
- Änderungen dieses Skriptes werden in orga-repo verwaltet, nicht im Webseiten-repo selbst. Siehe: `./build_system/git_hooks/post-receive`. Das Skript muss manuell aktualisiert werden siehe Befehl unten.
- Commits auf den master-Zweig bauen die Hauptwebseite. Force-Push führt zu Fehler!
- Commits auf andere Zweige bauen eine Vorschau, verfügbar unter: https://bubdd.uber.space/staging/{branchname}/
- <https://bubdd.uber.space/staging/> liefert eine Übersicht über alle bisher gebauten Vorschau-Zweige
- Vor einem Commit auf Master sollte unbedingt eine Vorschau gebaut werden. Ausnahme (kleine Tippfehler-Korrektur).
- Force-Push auf nicht-Master-Zweiwge sind zulässig. Sinnvoll, wenn es in der Entwurfsphase viele kleine Korrekturen gibt.
- Offene Frage/Problem: in der Vorschau funktionert noch nicht alles 100% (Vermutlich Weiterleitungen aus .htaccess).


## Hilfreiche Befehle:

- SSH-Passwort für bestimmte Zeit setzen:
    - eval $(ssh-agent); ssh-add -t 10m
- Hochladen von Dateien
    - `rsync -av manual_preview.sh bubdd@kojima.uberspace.de:/home/bubdd/tmp`
    - `rsync -av post-receive bubdd@kojima.uberspace.de:/home/bubdd/git/dresden.bits-und-baeume.org.git/hooks`
- inkonsistenten Zustand des Repos bereinigen:
    - Auf dem Server: `git reflog` -> ID raussuchen (wo repo noch in Ordnung war)
    - Auf dem Server: `git reset --hard <ID>`
    - lokal: `git commit --allow-empty -m "trigger build"; git push`


## Manuelle Vorschau von Zweigen

- Das Skript `./build_system/manual_preview.sh` dient dem manuellen triggern der Vorschau-Erstellung. Das kann von überall auf dem server aufgerufen werden. Aktuell liegt es in ~/tmp.




## Issues:

- In nicht-master-Zweigen werden Bilder nicht korrekt eingebunden (Pfadproblem).
    - der relative Pfad in der HTML-Datei ist: `/assets/images/2019-05-23/impressions_small_merge.jpg`
    - Daraus wird dann der Absolute Pfad: `https://bubdd.uber.space/assets/images/2019-05-23/impressions_small_merge.jpg`
    - Das müsste ins Template eingebaut werden: <https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base>
- Die Meta-Infos der Blogbeiträge sind noch nicht gut. Wichtig für Vorschau in sozialen Medien, Chats, etc.


## Zukunftsideen:

- niedrigschwelligerer Zugang für mehr Menschen (gitolite/codeberg)
- (Twitter-)-Cards im Template unterstützen https://developer.twitter.com/en/docs/tweets/optimize-with-cards/guides/getting-started
