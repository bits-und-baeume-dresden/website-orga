#!/bin/bash


echo -e "\n\n2020-03-19: This script should be obsolete. Website building is now triggered by post-receive hook for every branch.\n"
read -p "Press enter to continue"


# exit on every nonzero exit-code
set -e

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters. Exactly one argument required."
    exit 1
fi

BRANCHNAME=$1

echo "this is the BRANCHNAME: $BRANCHNAME"

DESTINATION="/var/www/virtual/bubdd/html/staging/$BRANCHNAME"
BASEURL="//bubdd.uber.space/staging/$BRANCHNAME"

echo "this is the DESTINATION: $DESTINATION"
echo "this is the BASEURL: $BASEURL"

# create target dir:

mkdir -p "$DESTINATION"

cd /home/bubdd/jekyll/dresden.bits-und-baeume.org || exit

echo "==== git ======================================================="

unset GIT_DIR


# the following construct allows to cope with force-pushes
# create local branch (no problem if it already exists)
git checkout -B $BRANCHNAME
FIRST_COMMIT_ID=$(git rev-list --max-parents=0 --abbrev-commit HEAD)
git reset --hard $FIRST_COMMIT_ID
git pull origin $BRANCHNAME

git checkout -f $BRANCHNAME

echo "==== bundle ===================================================="

bundle config set path 'vendor/bundle'
bundle update

echo "==== jekyll ===================================================="

# original build command
# bundle exec jekyll build --destination /var/www/virtual/bubdd/dresden.bits-und-baeume.org
bundle exec jekyll build --baseurl $BASEURL --destination $DESTINATION
